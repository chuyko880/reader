package com.company;

import java.io.*;

public class ReaderClass {
    private InputStream inputStream;
    private String pathin;

    public ReaderClass(String pathin) {
        this.pathin = pathin;
    }

    public void read() throws IOException {
        inputStream = new FileInputStream(pathin);
        int data = inputStream.read();
        char content;
        while (data != -1){
            content = (char) data;
            System.out.print(content);
            data = inputStream.read();
        }
        inputStream.close();
    }
}
